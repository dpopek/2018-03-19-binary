package homwork.binary;


public class AppMain {

    public static void main( String[] args ){

        int numberToChange = 15;
        int arraySize = 4;
        int[] arrayBinary = new int[arraySize];
        int arraySpot = arraySize - 1;
        System.out.println("Binary representation of "+ numberToChange + " is:");
        for (int i = 0; i < arraySize; i++){
            arrayBinary[arraySpot] = numberToChange % 2;
            numberToChange = numberToChange / 2;
            arraySpot--;
        }
        for (int item : arrayBinary) System.out.print(item) ;
        }
    }

